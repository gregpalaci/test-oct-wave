// https://next.tailwindcss.com/docs/configuration/#app
module.exports = {
  theme: {
    extend: {
      colors: {
        purple: 'rebeccapurple'
      }
    }
  },
  variants: {},
  plugins: []
}
